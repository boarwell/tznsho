package main

import (
	"path/filepath"
	"fmt"
	"os"
)

func main() {
	//os.Create("test.txt")

	cdir, _ := os.Getwd()
	fmt.Println(cdir)
	src, _ := filepath.Abs("./test.txt")
	dst, _ := filepath.Abs("./origin/test.txt")
	os.Rename(src, dst) 
	/*
	os.Create("hoge.xls")
	os.Create("fuga.xlsx")
	
	dirItems, _ := ioutil.ReadDir(cdir)
	r := regexp.MustCompile(`.xls$|.xlsx$`)
	
	for _, v := range dirItems {
		originPath, _ := filepath.Abs("./origin")
		os.Rename(v, originPath)
	}
	*/
}