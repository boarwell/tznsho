package main

import (
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
)

func makeOriginIfNotExists() {
	// originディレクトリが存在するか確認する
	if _, err := os.Stat("origin"); os.IsNotExist(err) {
		// mkdirのエラーは捨てます
		os.Mkdir("origin", 0755)
	}
}

func makeTznshoList() ([]string, []string) {
	// .xls, .xlsxのリストを作る
	cdir, _ := os.Getwd()
	dirItems, _ := ioutil.ReadDir(cdir)

	var xlsList []string
	var xlsxList []string
	xls := regexp.MustCompile(`.xls$`)
	xlsx := regexp.MustCompile(`.xlsx$`)

	for _, v := range dirItems {
		if xls.MatchString(v.Name()) {
			xlsList = append(xlsList, v.Name())
		} else if xlsx.MatchString(v.Name()) {
			xlsxList = append(xlsxList, v.Name())
		}
	}
	return xlsList, xlsxList
}

// GetIiname ...
// 拡張子の前に"_ii"とつける
func GetIiname(withExtension string) string {
	xls := regexp.MustCompile(`.xls$`)
	xlsx := regexp.MustCompile(`.xlsx$`)

	if xls.MatchString(withExtension) {
		byteName := []byte(withExtension)
		basenameB := byteName[0 : len(byteName)-4]
		basenameS := string(basenameB)
		iiname := basenameS + "_ii.xls"
		return iiname
	}

	if xlsx.MatchString(withExtension) {
		byteName := []byte(withExtension)
		basenameB := byteName[0 : len(byteName)-5]
		basenameS := string(basenameB)
		iiname := basenameS + "_ii.xlsx"
		return iiname
	}
	return withExtension
}

func copyOrigins(xlsList, xlsxList []string) {
	// リストにあるファイルをコピーする
	for _, v := range xlsList {
		// エラー処理はあとで実装しましょう
		origin, _ := os.Open(v)
		defer origin.Close()
		newname := GetIiname(v)
		// エラー処理はあとで実装しましょう
		iifile, _ := os.Create(newname)
		defer iifile.Close()
		// エラー処理はあとで実装しましょう
		io.Copy(iifile, origin)
	}

	for _, v := range xlsxList {
		// エラー処理はあとで実装しましょう
		origin, _ := os.Open(v)
		defer origin.Close()
		newname := GetIiname(v)
		// エラー処理はあとで実装しましょう
		iifile, _ := os.Create(newname)
		defer iifile.Close()
		// エラー処理はあとで実装しましょう
		io.Copy(iifile, origin)
	}

	// TODO
	// すでにoriginディレクトリにオリジナルがあるファイルはコピーしない（できれば）
}

func moveOrigins(xlsList, xlsxList []string) {
	// もとのファイルをoriginディレクトリに移動
	for _, v := range xlsList {
		originPath, _ := filepath.Abs("./origin")
		// dst := originPath + v    これだと"originhoge.xls"みたいになってしまう
		dst := originPath + "/" + v
		os.Rename(v, dst)
	}
	for _, v := range xlsxList {
		originPath, _ := filepath.Abs("./origin")
		// dst := originPath + v    これだと"originhoge.xls"みたいになってしまう
		dst := originPath + "/" + v
		os.Rename(v, dst)
	}
}

func main() {
	makeOriginIfNotExists()
	xlsList, xlsxList := makeTznshoList()
	copyOrigins(xlsList, xlsxList)
	moveOrigins(xlsList, xlsxList)
}
