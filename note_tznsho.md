# golangの存在確認について
- golangにはos.IsExist(err error), os.IsNotExist(err error)がある
- どちらもerrorを受け取って、そのエラーがどんなタイプなのかを判別する、という形を取っている
    - それがgolangの考え方っぽい
    - UNIXのようにエラーナンバーを返すのではなく、たくさんの情報を持った「エラー型」を返す。プログラマはそこからさまざまな情報を引き出すことができる
- os.IsExist()とos.IsNotExist()にはサンプルがあった
```
package main

import (
    "fmt"
    "os"
)

func main() {
        filename := "a-nonexistent-file"
            // os.Stat()とos.IsNotExist()の間のセミコロンは
            // 条件判定の前処理
            // os.Stat()でerrに値を代入して、それをIsNotExist()で判定
            if _, err := os.Stat(filename); os.IsNotExist(err) {
                        fmt.Printf("file does not exist")
                            }
}
```
- golangでの存在処理の考え方は以下の通りでしょうか
    1. os.Stat(filename)でfilenameの情報（os.FileInfo）とエラーを取得
    2. os.FileInfoは必要ないので`_`で捨てる
    3. os.Stat(filename)のエラーをos.IsNotExist(err)に食わせる
    4. ファイル、ディレクトリが存在するかどうかの真偽値が返ってくる

- os.IsNotExist(err)はエラーを引数に取る
    1. 何かしたい処理をする  
    2. エラーが返ってくる  
    3. エラーに対してIsNotExistをすることで、存在しなかったとわかる  

- エラーが起こりうる処理は、事前に大丈夫か確かめるのでなく、とりあえずやってみて、エラーが出たら処理をする、ということかね

### コード
```
type FileInfo interface {
            Name()       string
            Size()       int64
            Mode()       FileMode
            ModTime()    time.Time 
            IsDir()      bool
            Sys()        interface{}   
}

os.Stat(name string) (FileInfo, error)
os.IsNotExist(err error) bool

```

# ファイルのコピー
- コピーは`io.Copy(old, new \*File) `
- 実装は以下の感じになるでしょうか
```
old := os.Open(filename)    // コピーしたいファイルをオープン
defer src.Close()           // deferでファイルをクローズ
new := os.Create(newname)   // コピー先のファイルを作成、別のディレクトリも可能？
defer new.Close()
io.Copy(old, new)           //errorが返ってきます
```
- 別のディレクトリにコピーすることはできないみたい
- 別のディレクトリへはos.Rename(oldpath, newpath string)で移動
    - moveの関数はないみたい

### コード
```
os.Open(name string) (*File, error)        // openはread onlyで開く
os.Create(name string) (*File, error)
io.Copy(dst Writer, src Reader) (written int64, err error)
```

